'use strict';

const assert = require('assert'),
    child_process = require('child_process'),
    fs = require('fs'),
    url = require('url');

function _argsArray(args) {
    return Array.prototype.slice.call(args, 0);
}

function safe(funcOrPromise, options) {
    exports.error = null;
    const errorReturnValue = typeof options === 'object' && options !== null ? options.errorReturnValue : options;
    const debug = typeof options === 'object' && options !== null ? options.debug : null;

    try {
        let result;
        if (typeof funcOrPromise === 'function') {
            result = funcOrPromise();
            if (typeof result !== 'object' || typeof result.then !== 'function') return result; // result is thenable
        } else {
            result = funcOrPromise;
        }

        return new Promise((resolve) => {
            const onFulfilled = (x) => resolve([null, x]);
            const onRejected = (e) => { if (debug) debug(e); resolve([e]); };

            result.then(onFulfilled, onRejected); // assume result is a thenable and not a proper Promise
        });
    } catch (e) {
        exports.error = e;
        if (debug) debug(e);
        return typeof errorReturnValue === 'undefined' ? null : errorReturnValue;
    }
}

function jsonParse() {
    const args = _argsArray(arguments);
    return safe(function () { return JSON.parse.apply(JSON, args); }, null);
}

function jsonStringify() {
    const args = _argsArray(arguments);
    return safe(function () { return JSON.stringify.apply(JSON, args); }, null);
}

function csvParse(text, ...schema) {
    safe.error = null;

    if (typeof text !== 'string' || !Array.isArray(schema)) {
        safe.error = new Error('Argument error');
        return null;
    }

    const lines = text.split('\n');

    const fields = [];
    for (const item of schema) {
        const [name, type] = item.split(':');
        fields.push({ name, type: type || 'string' });
    }

    const result = [];
    for (const line of lines) {
        const values = line.split(',').map(v => v.trim());
        const data = {};
        fields.forEach((field, idx) => {
            const value = values[idx];
            if (!value) return;
            switch (field.type) {
            case 'string': data[field.name] = value; break;
            case 'boolean': {
                if (value !== 'true' && value !== 'false') return;
                data[field.name] = value === 'true';
                break;
            }
            case 'number': {
                const n = parseInt(value, 10);
                if (!Number.isFinite(n)) return;
                data[field.name] = n;
                break;
            }
            default: break;
            }
        });
        result.push(data);
    }
    return result;
}

function openSync() {
    const args = _argsArray(arguments);
    return safe(function () { return fs.openSync.apply(fs, args); }, -1);
}

function closeSync() {
    const args = _argsArray(arguments);
    return safe(function () { return fs.closeSync.apply(fs, args); }, 0);
}

function readFileSync() {
    const args = _argsArray(arguments);
    return safe(function () { return fs.readFileSync.apply(fs, args); }, null);
}

function writeFileSync() {
    const args = _argsArray(arguments);
    return safe(function () { return fs.writeFileSync.apply(fs, args); }) !== null;
}

function statSync() {
    const args = _argsArray(arguments);
    return safe(function () { return fs.statSync.apply(fs, args); }, null);
}

function lstatSync() {
    const args = _argsArray(arguments);
    return safe(function () { return fs.lstatSync.apply(fs, args); }, null);
}

function readdirSync() {
    const args = _argsArray(arguments);
    return safe(function () { return fs.readdirSync.apply(fs, args); }, null);
}

// afaik, this never throws
function existsSync() {
    const args = _argsArray(arguments);
    return safe(function () { return fs.existsSync.apply(fs, args); }, false);
}

function mkdirSync() {
    const args = _argsArray(arguments);
    return safe(function () { return fs.mkdirSync.apply(fs, args); }) !== null;
}

function chownSync() {
    const args = _argsArray(arguments);
    return safe(function () { return fs.chownSync.apply(fs, args); }) !== null;
}

function unlinkSync() {
    const args = _argsArray(arguments);
    return safe(function () { return fs.unlinkSync.apply(fs, args); }) !== null;
}

function rmdirSync() {
    const args = _argsArray(arguments);
    return safe(function () { return fs.rmdirSync.apply(fs, args); }) !== null;
}

function rmSync() {
    const args = _argsArray(arguments);
    return safe(function () { return fs.rmSync.apply(fs, args); }) !== null;
}
function chmodSync() {
    const args = _argsArray(arguments);
    return safe(function () { return fs.chmodSync.apply(fs, args); }) !== null;
}

function readlinkSync() {
    const args = _argsArray(arguments);
    return safe(function () { return fs.readlinkSync.apply(fs, args); }, null);
}

function realpathSync() {
    const args = _argsArray(arguments);
    return safe(function () { return fs.realpathSync.apply(fs, args); }, null);
}

function renameSync() {
    const args = _argsArray(arguments);
    return safe(function () { return fs.renameSync.apply(fs, args); }) !== null;
}

function readSync() {
    const args = _argsArray(arguments);
    return safe(function () { return fs.readSync.apply(fs, args); }) !== null;
}

function appendFileSync() {
    const args = _argsArray(arguments);
    return safe(function () { return fs.appendFileSync.apply(fs, args); }) !== null;
}

function urlParse() {
    const args = _argsArray(arguments);
    return safe(function () { return url.parse.apply(url, args); }, null);
}

function safeRequire() {
    const args = _argsArray(arguments);
    return safe(function () { return require.apply(null, args); }, null);
}

function execSync() {
    const args = _argsArray(arguments);
    return safe(function () { return child_process.execSync.apply(child_process, args); }, null);
}

function spawnSync() {
    const args = _argsArray(arguments);
    return safe(function () { return child_process.spawnSync.apply(child_process, args); }, null);
}

function pick(obj, ...fields) {
    safe.error = null;

    if (!obj || typeof obj !== 'object') {
        safe.error = new Error('obj has incorrect type');
        return null;
    }

    const result = {};
    for (const field of fields) {
        const [fieldName, type] = field.split(':');
        if (!(fieldName in obj)) continue;
        if (type && typeof obj[fieldName] !== type) {
            safe.error = new Error(`${fieldName} has incorrect type, expecting ${type}`);
            return null;
        }
        result[fieldName] = obj[fieldName];
    }
    return result;
}

// http://stackoverflow.com/questions/6491463
// currently, '.' is assumed to be the separator
function query(o, s, defaultValue) {
    if (!s) return o;

    assert(typeof s === 'string' || Array.isArray(s));

    let a;
    if (Array.isArray(s)) { // already split up
        a = s;
    } else {
        s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
        s = s.replace(/^\./, '');           // strip a leading dot

        a = s.split('.'); // always returns an array
    }

    for (let i = 0; i < a.length; i++) {
        const n = a[i];

        if (!o || typeof o !== 'object' || !(n in o)) return defaultValue;

        o = o[n];
    }
    return o;
}

// TODO: support array format like [0].some.value
function set(o, s, value) {
    if (!s) return o;

    assert(typeof s === 'string' || Array.isArray(s));

    if (!o || typeof o !== 'object') o = { };

    let a;
    if (Array.isArray(s)) {
        a = s;
    } else {
        s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
        s = s.replace(/^\./, '');           // strip a leading dot

        a = s.split('.'); // always returns an array
    }

    let io = o;
    for (let i = 0; i < a.length - 1; i++) {
        const n = a[i];

        if (!(n in io) || !io[n] || typeof io[n] !== 'object') {
            io[n] = { };
        }

        io = io[n];
    }

    io[a[a.length - 1]] = value;

    return o;
}

function unset(o, s) {
    if (!s) return o;

    assert(typeof s === 'string' || Array.isArray(s));

    if (!o || typeof o !== 'object') return o;

    let a;
    if (Array.isArray(s)) {
        a = s;
    } else {
        s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
        s = s.replace(/^\./, '');           // strip a leading dot

        a = s.split('.'); // always returns an array
    }

    let io = o;
    for (let i = 0; i < a.length - 1; i++) {
        const n = a[i];

        if (!(n in io)) return o;

        if (!io[n] || typeof io[n] !== 'object') {
            delete io[n];
            return o;
        }

        io = io[n];
    }

    delete io[a[a.length - 1]];

    return o;
}

Object.assign(safe, {
    JSON: {
        parse: jsonParse,
        stringify: jsonStringify
    },

    CSV: {
        parse: csvParse,
    },

    fs: {
        openSync,
        closeSync,
        readSync,
        readFileSync,
        writeFileSync,
        statSync,
        lstatSync,
        existsSync,
        mkdirSync,
        rmdirSync,
        rmSync,
        unlinkSync,
        renameSync,
        readdirSync,
        chmodSync,
        readlinkSync,
        realpathSync,
        appendFileSync,
        chownSync
    },

    child_process: {
        execSync,
        spawnSync
    },

    require: safeRequire,

    url: {
        parse: urlParse
    },

    pick,
    query,
    set,
    unset,

    safeCall: safe,
    safeAwait: safe,

    error: null
});

exports = module.exports = safe;
